# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://amadejj@bitbucket.org/amadejj/stroboskop.git 
```

Naloga 6.2.3:
https://bitbucket.org/amadejj/stroboskop/commits/7dcf9877258b55579f2daee3b1e9b81028408bc8

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/amadejj/stroboskop/commits/5839c92d0e585f2a152c9aaf2b4008fb8a6ef9b5

Naloga 6.3.2:
https://bitbucket.org/amadejj/stroboskop/commits/cd562beb97086a7c561c925077ec51d111e89142

Naloga 6.3.3:
https://bitbucket.org/amadejj/stroboskop/commits/eddc9d7e259d34679d841ffb797f44df418e6e5f

Naloga 6.3.4:
https://bitbucket.org/amadejj/stroboskop/commits/3ba336895aa010372e31ffe35336228b7b1f6aa6

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/amadejj/stroboskop/commits/fa50bfa5c6de532da6e039abd0c0cf371e127ddf

Naloga 6.4.2:
https://bitbucket.org/amadejj/stroboskop/commits/9e93195e1269cdc9138915beced9d5e90d7551de

Naloga 6.4.3:
https://bitbucket.org/amadejj/stroboskop/commits/5ce9bbd644578e836cbd2157a760bdad272f7fd6

Naloga 6.4.4:
https://bitbucket.org/amadejj/stroboskop/commits/59e39011a3a6e4784f5a22db142379085a17b78d